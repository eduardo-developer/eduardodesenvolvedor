import 'dotenv/config'

import cors from 'cors'
import express from 'express'
import bodyParser from 'body-parser'
import mongoose from 'mongoose'
import path from 'path'
import { fileURLToPath } from 'url'

import projectRoutes from './src/routes/projects.js'

const __dirname = path.dirname(fileURLToPath(import.meta.url))

const app = express()

app.use(bodyParser.json({ limit: "30mb", extended: true }))
app.use(bodyParser.urlencoded({ limit: "30mb", extended: true }))
app.use(cors())

app.use('/projects', projectRoutes)

app.use(express.static(path.join(__dirname, "./ui/build")))

app.get("*", function(_, res) {
   res.sendFile(
       path.join(__dirname, "./ui/build/index.html"),
       function(err) {
           if (err) {
               res.status(500).send(err)
           }
       }
   )
})

const DB_USER = process.env.DB_USER
const DB_PASSWORD = process.env.DB_PASSWORD

mongoose.connect(`mongodb+srv://${DB_USER}:${DB_PASSWORD}@cluster0.ee1ue.mongodb.net/projects?retryWrites=true&w=majority`, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => app.listen(process.env.PORT || 5000, () => console.log(`Server Running`)))
    .catch((error) => console.log(`${error} did not connect`))

