import { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'

import { getProjects } from '../../actions/project'
import { ViewProjectsAdmin } from '../../components/ViewProjectsAdmin'
import { useAuth0 } from '@auth0/auth0-react'

const KEY_ADMIN = process.env.REACT_APP_KEY_ADMIN

export const AdminPage = () => {
    const [ currentId, setCurrentId ] = useState(0)

    const { user } = useAuth0()

    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getProjects())
    }, [dispatch])

    return user.sub == KEY_ADMIN && 
            <ViewProjectsAdmin currentId={currentId} setCurrentId={setCurrentId}/>
}

