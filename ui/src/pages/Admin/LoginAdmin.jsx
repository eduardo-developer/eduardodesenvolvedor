import { useAuth0 } from '@auth0/auth0-react'
import { AdminPage } from './AdminPage'

export const LoginAdmin = () => {
    const { loginWithRedirect, isAuthenticated } = useAuth0()

    return !isAuthenticated ? (
                <button onClick={() => loginWithRedirect()}>
                    login
                </button>
           ) : (
            <AdminPage />
        )
}

