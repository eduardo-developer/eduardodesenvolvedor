import { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { Categories } from '../components/Categories'
import { ViewProjects } from '../components/ViewProjects'
import { Title } from '../components/Title'

import { getProjects } from '../actions/project'

import { portfolios } from '../components/allPortfolios'
import { useEffect } from 'react'

const allCategories = ['All', ...new Set(portfolios.map(item => item.category))]

export const ProjectsPage = () => {
    const projects = useSelector((state) => state.projects)

    const [currentId, setCurrentId] = useState(0)
    const dispatch = useDispatch()

    const [categories, setCategories] = useState(allCategories)
    const [menuItems, setMenuItems] = useState(portfolios)

    useEffect(() => {
        dispatch(getProjects())
    }, [currentId, dispatch])

    const filter = (category) => {
        if(category === 'All') {
            setMenuItems(portfolios)
            return
        }
        const filteredData = portfolios.filter((item) => {
            return item.category === category
        })
        setMenuItems(filteredData)

    }

    return (
        <div className="PortfolioPage">
            <div className="title">
                <Title title={'Projetos'} span={'Projetos'}/>
            </div>
            <div className="portfolios-data">
                <Categories filter={filter} categories={categories} />
                <ViewProjects setCurrentId={setCurrentId} />
            </div>
        </div>
    )

}

