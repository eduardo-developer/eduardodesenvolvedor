import { Link } from 'react-router-dom'

import { AiFillGithub, AiFillLinkedin, AiOutlineMail } from 'react-icons/ai'

export const HomePage = () => {
    return (
        <div className="HomePage">
            <header className="hero">

                <h1 className="hero-text">
                    Olá sou
                    <span> Eduardo Silva</span>
                </h1>
                <p className="h-sub-text">
                    Desenvolvedor autodidata entusiasmado, buscando uma posição de nível de entrada,solução de problemas  e auxiliando na conclusão oportuna dos projetos.
                </p>
                <div className="icons">
                    <a href="https://github.com/eduardo-sdev" target="_blank" rel="noopener noreferrer">
                        <AiFillGithub />
                    </a>
                    <a href="https://github.com/eduardo-sdev" target="_blank" rel="noopener noreferrer">
                        <AiFillLinkedin />
                    </a>
                    <a href="https://github.com/eduardo-sdev" target="_blank" rel="noopener noreferrer">
                        <AiOutlineMail />
                    </a>
                </div>
            </header>
        </div>
    )
}

