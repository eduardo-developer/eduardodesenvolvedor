import { ContactItem } from '../components/ContactItem'

import { Title } from '../components/Title'

import { AiOutlineWhatsApp, AiOutlineMail } from 'react-icons/ai'

export const ContactPage = () => {
    return (
        <div>
            <div className="title">
                <Title title={'Contato'} span={'Contato'}/>
            </div>
            <div className="ContactPage">
                <div className="map-sect">

                </div>
                <div className="contact-sect">
                    <ContactItem icon={<AiOutlineWhatsApp />} text={"(81) 99350-4951"} title={'Whatssapp'}/>
                    <ContactItem icon={<AiOutlineMail />} text={"eduardosilvaprogramador@gmail.com "} title={'Email'}/>
                </div>
            </div>
        </div>
    )
}

