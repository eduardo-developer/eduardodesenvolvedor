import React from 'react'
import ReactDOM from 'react-dom'

import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'

import { reducers } from './reducers'
import App from './App'

import Auth0ProviderWithHistory from './auth/auth0-provider-with-history';

const store = createStore(reducers, compose(applyMiddleware(thunk)));

ReactDOM.render(
  <React.StrictMode>
        <Auth0ProviderWithHistory>
            <Provider store={store}>
                <App />
            </Provider>
        </Auth0ProviderWithHistory>
    </React.StrictMode>,
  document.getElementById('root'),
)

