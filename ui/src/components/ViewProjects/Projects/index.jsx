import moment from 'moment'

export const Projects = ({project}) => {
    return project.status === 'public' && (
        <>
            <div className="portfolio" key={project._id}>
                    <div className="image-data">
                        <img src={project.selectedFile || 'https://user-images.githubusercontent.com/194400/49531010-48dad180-f8b1-11e8-8d89-1e61320e1d82.png'} alt=""/>
                        <ul className="hover-items">
                            <li>
                                <a href="">Sobre</a>
                                <a href="">Demo</a>
                            </li>
                        </ul>
                    </div>
                    <h3>
                        {project.title}
                    </h3>
                    <p>
                        {moment(project.createdAt).fromNow()}
                    </p>

                    <p>Placeholder</p>
                    </div>
        </>
    )
}

