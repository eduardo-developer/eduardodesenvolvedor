import { useSelector } from "react-redux"

import { Projects } from './Projects'

const Load = () => <img src="/img/load.gif" alt="load" />

export const ViewProjects = ({ setCurrentId }) => {
    const projects = useSelector((state) => state.projects)
    return (
        !projects.length ? <Load /> : (
            <div className="portfolis">
                {
                    projects.map(item =>
                        <div key={item._id} className="items">
                            <Projects project={item} setCurrentId={setCurrentId} />
                        </div>
                    )
                }
            </div>
        )
    )
}
