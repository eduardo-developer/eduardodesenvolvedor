import React from 'react'
import { NavLink } from 'react-router-dom'

export const Navbar = () => {
    return (
        <nav className='NavBar'>
            <div className='nav'>
                <ul className='nav-items'>
                    <li className='nav-item'>
                        <NavLink to="/" className={(navData) => navData.isActive ? "active" : ""}>
                            Home
                        </NavLink>
                    </li>
                    <li className='nav-item'>
                        <NavLink to="/project">
                            Projetos
                        </NavLink>
                    </li>
                    <li className='nav-item'>
                        <NavLink to="/contact">
                            Contato
                        </NavLink>
                    </li>
                </ul>
                <footer className='footer'>
                    <NavLink to="/admin">
                        @2022
                    </NavLink>
                </footer>
            </div>
        </nav>
    )
}

