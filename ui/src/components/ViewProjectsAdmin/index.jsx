import { useEffect, useState } from 'react'
import moment from 'moment'

import { useDispatch, useSelector } from 'react-redux'

import FileBase from 'react-file-base64'

import { createProject, updateProject, deleteProject } from '../../actions/project'

import './style.sass'

const Load = () => <img src="/img/load.gif" alt="load" />

const FormProject = ({ currentId, setCurrentId }) => {
    const [projectData, setProjectData] = useState({
        title: '',
        status: '',
        description: '',
        tags: '',
        selectedFile: '',
    })

    const project = useSelector((state) => (currentId ? state.projects.find((message) => message._id === currentId) : null))

    const dispatch = useDispatch()

    useEffect(() => {
        if(project) setProjectData(project)
    }, [project])

    const clear = () => {
        setCurrentId(0);
        setProjectData({ title: '', status: '', description: '', tags: '', selectedFile: '' });
      }

    const handleSubmit = async (event) => {
        event.preventDefault()

        if(currentId === 0){
            dispatch(createProject(projectData))
            clear()
        } else {
            dispatch(updateProject(currentId, projectData))
            clear()
        }
    }

    return (
        <div className="form-admin">
            <form noValidate onSubmit={handleSubmit}>
               
               <label>title</label>
                <input type="text" defaultValue={projectData.title} onChange={(e) => setProjectData({ ...projectData, title: e.target.value})} />

               <label>status</label>

               {/* <input type="text" defaultValue={projectData.status} onChange={(e) => setProjectData({ ...projectData, status: e.target.value})} /> */}
               <select onChange={(e) => setProjectData({ ...projectData, status: e.target.value })}>
                  <option defaultValue=''> </option>
                  <option defaultValue='private'>private</option>
                  <option defaultValue='public'>public</option>
                </select>

               <label>description</label>
                <input type="text" defaultValue={projectData.description} onChange={(e) => setProjectData({ ...projectData, description: e.target.value})} />

               <label>tags</label>
                <input type="text" defaultValue={projectData.tags} onChange={(e) => setProjectData({ ...projectData, tags: e.target.value.split(',')})} />

               <label>upload image</label>
                <div>
                    <FileBase
                        type="file"
                        multiple={false}
                        onDone={({base64}) => setProjectData({ ...projectData, selectedFile: base64})}
                    />
                </div>
                <input type="submit" value="Novo Projeto" />
            </form>

        </div>
    )
}

const Projects = ({project, setCurrentId}) => {
    const dispatch = useDispatch()
    return (
        <>
            <div className="portfolio" key={project._id}>
                <div>{project.status}</div>
                    <div className="image-data">
                        <img src={project.selectedFile || 'https://user-images.githubusercontent.com/194400/49531010-48dad180-f8b1-11e8-8d89-1e61320e1d82.png'} alt=""/>
                    </div>
                    <h3>
                        {project.title}
                    </h3>
                    <p>
                        {moment(project.createdAt).fromNow()}
                    </p>
                <button onClick={() => setCurrentId(project._id)} >edit</button>
                <button onClick={() => dispatch(deleteProject(project._id))}>delete</button>
            </div>
        </>
    )
}

export const ViewProjectsAdmin = ({ currentId, setCurrentId }) => {
    const projects = useSelector((state) => state.projects)

    return (
        <>
            <FormProject currentId={currentId} setCurrentId={setCurrentId} />
                {
                    !projects.length ? <Load /> : (
                        <div className="portfolis">
                            {
                                projects.map(item => 
                                    <div key={item._id} className="items">
                                        <Projects project={item} setCurrentId={setCurrentId} />
                                    </div>
                                )
                            }
                        </div>
                    )
                }
        </>
    )
}

