export const ContactItem = ({ icon, text, title }) => {
    return (
        <div className="ContactItem">
            <div className="contact">
                <div className="contact-icon">
                    {icon} 
                </div>
                <div className="right-items">
                    <h6>{title}</h6>
                    <p>{text}</p>
                </div>
            </div>
        </div>
    )
}

