import axios from 'axios'

const API = axios.create({ 
    baseURL: 'http://localhost:5000' 
})

export const fetchProjects = () => API.get('/projects')
export const createProject = (newProject) => API.post('projects', newProject)
export const updateProject = (id, updateProject) => API.patch(`/projects/${id}`, updateProject)
export const deleteProject = (id) => API.delete(`/projects/${id}`)

