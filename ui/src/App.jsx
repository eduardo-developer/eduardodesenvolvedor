import { useEffect, useState } from 'react'
import './style.sass'

import { useDispatch } from 'react-redux'

import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom"

import { getProjects } from './actions/project'

import { HomePage } from './pages/HomePage'
import { ProjectsPage } from './pages/ProjectsPage'
import { ContactPage } from './pages/ContactPage'

import { LoginAdmin } from './pages/Admin/LoginAdmin'

import { Navbar } from './components/index'


const app = () => {
    const dispatch = useDispatch()

    useEffect(() => {
      dispatch(getProjects())
    }, [dispatch])

    const [navToggle, setNavToggle] = useState(false)
    
    const navClick = () => {
        setNavToggle(!navToggle)
    }

    return (
     <BrowserRouter>
            <div className="App">
                <div className={`sidebar ${navToggle ? 'nav-toggle': ''}`}>
                    <Navbar />
                </div>
                <div className='nav-btn' onClick={navClick}>
                    <div className='lines-1'></div>
                    <div className='lines-2'></div>
                    <div className='lines-3'></div>
                </div>
                <div className='main-content'>
                    <div className='content'>
                      <Routes>
                        <Route path="/" element={<HomePage />} />
                        <Route path="/project" element={<ProjectsPage />} />
                        <Route path="/contact" element={<ContactPage />} />
                        <Route path="/admin" element={<LoginAdmin />} />
                      </Routes>
                    </div>
                </div>
            </div>
    </BrowserRouter>
    )
}

export default app

