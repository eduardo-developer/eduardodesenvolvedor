<h1 align="center">
    <img alt="Portfolio eduardo silva" title="Portfolio eduardo silva" src=".github/logo.svg" />
</h1>

<p align="center">
  <a href="#-tecnologias">Tecnologias</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#-projeto">Projeto</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#-layout">Layout</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#memo-licença">Licença</a>
</p>


<p align="center">
  <img alt="Portfolio eduardo silva" src=".github/banner.png" width="100%">
</p>

## 🚀 Tecnologias

Tecnologias usadas:

> front-end
- Reactjs
- Sass
- Auth0

> back-end
- Express

## 💻 Projeto

O projeto é uma aplicação que apresenta projetos web

## 🔖 Layout

- [Layout](https://www.figma.com/)

## :memo: Licença

Esse projeto está sob a licença MIT. Veja o arquivo [LICENSE](LICENSE.md) para mais detalhes.

---
