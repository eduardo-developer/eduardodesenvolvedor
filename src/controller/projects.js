import express from 'express'
import mongoose from 'mongoose'
import PostProject from "../models/postProject"

const router = express.Router()

export const getProjects = async (req, res) => {
    try {
        const postProject = await PostProject.find()

        res.status(200).json(postProject)
    } catch (error) {
        res.status(404).json({ message: error.message })
    }
}

export const createProject = async (req, res) => {
    const project = req.body

    const newProject = new PostProject({ ...project, createdAt: new Date().toISOString() })

    try {
        await newProject.save()

        res.status(201).json(newProject)
    } catch (error) {
        res.status(409).json({ message: error.message })
    }
}

export const updateProject = async (req, res) => {
    const { id } = req.params
    const { title, status, description, selectedFile, tags}  = req.body

    if(!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send('no project with id')
    
    const updateProject = { title, status, description, tags, selectedFile, _id: id };

    await PostProject.findByIdAndUpdate(id, updateProject, { new: true });

    res.json(updateProject)
}

export const deleteProject = async (req, res) => {
    const { id } = req.params

    if(!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send('no project with id')

    await PostProject.findByIdAndRemove(id)

    res.json({ message: 'Project deleted successfully'})
}

export default router

